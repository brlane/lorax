12/2/2020 bcl

lorax is now being manually updated from the rhel9-branch of the project, found here:
https://github.com/weldr/lorax

Builds will have .el9 added to their tag to differentiate them from Fedora builds.

Eventually we will branch from fedora and start using a .Z version the same way we do for RHEL8,
but currently we are not doing that.
